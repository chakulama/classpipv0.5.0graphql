import { TestBed, inject } from '@angular/core/testing';

import { GroupGraphqlService } from './group-graphql.service';

describe('GroupGraphqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupGraphqlService]
    });
  });

  it('should be created', inject([GroupGraphqlService], (service: GroupGraphqlService) => {
    expect(service).toBeTruthy();
  }));
});
